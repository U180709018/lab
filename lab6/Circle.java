public class Circle {
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }


    public double area(){
        return Math.PI * radius * radius;
    }

    public double perimeter(){
        return Math.PI * radius * 2;
    }

    public boolean intersect(Circle c){
        return radius + c.radius > center.distance(c.center);

    }
}


